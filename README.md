# Munstrap
Un thème *francophone* pour [Munin 2.x](http://munin-monitoring.org/), basé sur [Twitter Bootstrap](http://getbootstrap.com) et grandement inspiré de [munstrap](https://github.com/jonnymccullagh/munstrap).

![Aperçu](https://bitbucket.org/lbesson/munstrap/raw/master/sample.png "Un exemple")

## Installation par étapes
> Cela requiert bien sûr d'avoir Munin installé, et s'applique à un système GNU/Linux classique, style Debian ou Ubuntu.

1. Installer Munstrap:

   ```bash
   cd /etc/munin
   sudo git clone https://bitbucket.org/lbesson/munstrap.git
   ```

2. Sauvegarder les deux dossiers ``themes`` et ``static`` :

   ```bash
   sudo mv templates templates~
   sudo mv static static~
   ```

3. Remplacer le thème par défaut de Munin par le nouveau Munstrap (sans inquiétude, comme il y a une sauvegarde des vieux fichiers) :

   ```bash
   cp -rb munstrap/templates .
   cp -rb munstrap/static .
   ```

4. Nettoyer les vieilles pages HTML :

   ```bash
   rm -rf /var/www/munin/*
   ```

5. Attendre quelques minutes que les nouvelles pages HTML soient générées par Munin (ou [forcer leur génération](https://doc.ubuntu-fr.org/munin#mise_a_jour_des_indicateurs_des_n%C5%93uds) avec ``sudo -u munin /usr/bin/munin-cron``).

## Mises à jour
L'avantage d'avoir cloné le dépôt git est la possibilité de mettre à jour tout simplement par :

```bash
cd /etc/munin/munstrap
git pull
```

Et pour remplacer la vieille version par la nouvelle :

```bash
cd /etc/munin
rm -rf templates static
cp -r munstrap/templates .
cp -r munstrap/static .
```

## Retourner au thème par défaut
La sauvegarde faite au début est là pour ça :

```bash
cd /etc/munin
rm -rf templates static
mv templates~ templates
mv static~ static
```

## Exemples
Voir un nœud spécifique :
![Node view](https://bitbucket.org/lbesson/munstrap/raw/master/sample-node.png "Un exemple de nœud")

Comment zoomer (si le greffon zoom via *perl* en mode dynamique est activé) :
![Zoom view](https://bitbucket.org/lbesson/munstrap/raw/master/sample-zoom.png "Un exemple de zoom")

## Encore plus d'exemples
Voir la liste des avertissements :
![Problem view](https://bitbucket.org/lbesson/munstrap/raw/master/problem.png "List d'avertissements")

Exemple de page d'accueil :
![Home view](https://bitbucket.org/lbesson/munstrap/raw/master/home.png "Un exemple de page d'accueil")

----

## Licence
Ce projet est placé sous les termes de la **Licence GPLv3**,
pour plus de détails, veuillez vous référer au fichier [LICENSE](http://besson.qc.to/LICENSE.html) distribué avec le projet.

*En gros, ça vous permet d'utiliser le projet ou un de ces composants dans vos propres projets.*
